# Copyright 2021 Kal-It (https://kal-it.fr)
# @author Timothée Ringeard <timothee.ringeard@kal-it.fr>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html)


from odoo import http


class FirebaseAuthController(http.Controller):
    @http.route("/firebase/auth/db_list", type="json", auth="none")
    def firebase_db_list(self):
        return http.db_list(force=True)
