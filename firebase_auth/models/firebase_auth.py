# Copyright 2021 Kal-It (https://kal-it.fr)
# @author Timothée Ringeard <timothee.ringeard@kal-it.fr>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html)

import json
import logging
import os

from odoo import api, models

_logger = logging.getLogger(__name__)

try:
    import firebase_admin
    from firebase_admin import auth
    from firebase_admin import credentials
except ImportError:
    firebase_admin = None
    auth = None
    credentials = None
    _logger.error("Cannot import firebase_admin.")


cred_data = os.environ.get("FIREBASE_AUTH_CREDENTIALS", False)

if not cred_data:
    _logger.error("The FIREBASE_AUTH_CREDENTIALS env variable was not found")
else:
    try:
        cred_data = json.loads(cred_data)
        cred = credentials.Certificate(cred_data)
        firebase_admin.initialize_app(cred)
    except Exception as e:
        _logger.error(
            "Error during the firebase app initialization "
            "create_firebase_token (Firebase - Auth) : %s",
            e,
        )
        pass


class FirebaseAuth(models.TransientModel):
    _name = "firebase.auth"
    _description = "Firebase Auth"

    @api.model
    def create_firebase_token(self, uid):
        token = False
        if firebase_admin._apps:
            try:
                token = auth.create_custom_token(str(uid)).decode("utf-8")
            except Exception as e:
                _logger.error(
                    "Error during the token generation in "
                    "create_firebase_token (Firebase - Auth) : %s",
                    e,
                )
                pass
        return token
