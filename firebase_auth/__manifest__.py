# Copyright 2021 Kal-IT
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html)

{
    "name": "Firebase - Authentication",
    "version": "12.0.1.0.0",
    "author": "Kal-It",
    "license": "AGPL-3",
    "category": "Extra Tools",
    "website": "https://kal-it.fr",
    "depends": [
        "firebase_core",
    ],
    "data": [
        "views/res_config_settings_views.xml",
    ],
    "external_dependencies": {
        "python": ["firebase_admin"],
    },
}
