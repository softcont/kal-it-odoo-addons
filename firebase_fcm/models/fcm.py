# Copyright 2021 Kal-It (https://kal-it.fr)
# @author Timothée Ringeard <timothee.ringeard@kal-it.fr>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html)

import logging

from odoo import _, api, models
from odoo.exceptions import UserError

_logger = logging.getLogger(__name__)

try:
    from pyfcm import FCMNotification
    from pyfcm.errors import FCMError
except ImportError:
    FCMNotification = None
    FCMError = None
    _logger.error("Cannot import pyfcm.")


class FCM(models.TransientModel):
    _name = "firebase.fcm"
    _description = "Firebase - FCM Notifications"

    @api.model
    def _get_server_key(self):
        return (
            self.env["ir.config_parameter"]
            .sudo()
            .get_param("firebase.fcm.server_key", default=False)
        )

    @api.model
    def _connect_to_fcm(self):
        server_key = self._get_server_key()
        if not server_key:
            _logger.error(
                "Firebase - FCM Notifications : No server key "
                "has been defined."
            )
            raise UserError(
                _(
                    "You must set a server key in order to use "
                    "Firebase - FCM Notifications"
                )
            )
        try:
            fcm_service = FCMNotification(server_key)
            return fcm_service
        except FCMError as e:
            _logger.error(
                "Error during the connection to "
                "Firebase - FCM Notifications: %s",
                e,
            )
            raise UserError(
                _(
                    "Error during the connection to "
                    "Firebase - FCM Notifications : %s" % e
                )
            )

    @api.model
    def test_notification(self, registration_id, message_title, message_body):
        try:
            fcm_service = self._connect_to_fcm()
            result = fcm_service.notify_single_device(
                registration_id=registration_id,
                message_title=message_title,
                message_body=message_body,
            )
            return result
        except FCMError as e:
            _logger.error(
                "Error during the connection to "
                "Firebase - FCM Notifications : %s",
                e,
            )
            raise UserError(
                _(
                    "Error during the connection to "
                    "Firebase - FCM Notifications : %s" % e
                )
            )

    @api.model
    def send_notifications(
        self, registration_ids, message_title, message_body
    ):
        try:
            fcm_service = self._connect_to_fcm()
            fcm_service.notify_multiple_devices(
                registration_ids=registration_ids,
                message_title=message_title,
                message_body=message_body,
            )
        except FCMError as e:
            _logger.error(
                "Error during the sending of "
                "Firebase - FCM Notifications: %s",
                e,
            )
            raise UserError(
                _(
                    "Error during the sending of "
                    "Firebase - FCM Notifications: %s" % e
                )
            )
