# Copyright 2021 Kal-It (https://kal-it.fr)
# @author Timothée Ringeard <timothee.ringeard@kal-it.fr>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html)

from odoo import fields, models


class ResConfigSettings(models.TransientModel):
    _inherit = "res.config.settings"

    fcm_server_key = fields.Char(
        string="Server Key", config_parameter="firebase.fcm.server_key",
    )
