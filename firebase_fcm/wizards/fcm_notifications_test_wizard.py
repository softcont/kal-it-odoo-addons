# Copyright 2021 Kal-It (https://kal-it.fr)
# @author Timothée Ringeard <timothee.ringeard@kal-it.fr>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html)

from odoo import _, fields, models
from odoo.exceptions import UserError


class FCMTestNotification(models.TransientModel):
    _name = "firebase.fcm.test.notification"
    _description = "Firebase - FCM Test notification"

    device_registration_token = fields.Char(
        "Device Registration Token", required=True,
    )
    message_title = fields.Char("Message Title", required=True,)
    message_body = fields.Text("Message Body", required=True,)

    def test_fcm_notification(self):
        result = self.env["firebase.fcm"].test_notification(
            registration_id=self.device_registration_token,
            message_title=self.message_title,
            message_body=self.message_body,
        )
        if result["failure"] == 1:
            raise UserError(
                _("The test was not successful : %s" % result["results"])
            )
        else:
            raise UserError(
                _("The test was successful : %s" % result["results"])
            )
