# Copyright 2020 Kal-It (https://kal-it.fr)
# @author Timothée Ringeard <timothee.ringeard@kal-it.fr>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html)

import os
from distutils.util import strtobool

import elasticapm

import odoo
from odoo.tools.config import config


def is_true(strval):
    return bool(strtobool(strval or '0'.lower()))


elasticapm.instrument()

apm_env = os.environ.get("ELASTIC_APM_ENVIRONMENT", config.get("running_env"))
apm_host = os.environ.get("ELASTIC_APM_SERVER_URL", "http://localhost:8200")
odoo_version = odoo.release.version
apm_client = None
if is_true(os.environ.get("ELASTIC_APM")):
    apm_client = elasticapm.Client(
        service_name=os.environ.get("ELASTIC_APM_NAME"),
        server_url=apm_host,
        secret_token=os.environ.get("ELASTIC_APM_SECRET_TOKEN"),
        framework_name="Odoo",
        framework_version=odoo_version,
        environment=apm_env,
    )
