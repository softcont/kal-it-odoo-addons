# Copyright 2020 Kal-It
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html)
{
    "name": "Elastic APM",
    "version": "12.0.1.0.0",
    "author": "Kal-It",
    "license": "AGPL-3",
    "category": "Extra Tools",
    "website": "https://kal-it.fr",
    "depends": [
        "base",
    ],
    "data": [],
    "external_dependencies": {
        "python": ["elasticapm"],
    },
    "post_load": False
}
