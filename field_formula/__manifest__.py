# Copyright 2021 Kal-IT
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html)

{
    "name": "Field Formula",
    "version": "12.0.1.0.0",
    "author": "Kal-It",
    "license": "AGPL-3",
    "category": "Extra Tools",
    "website": "https://kal-it.fr",
    "depends": [
        "web",
    ],
    "data": [
        "views/assets.xml",
    ],
}
